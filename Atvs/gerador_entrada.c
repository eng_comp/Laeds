/****************************************
Laed1 - Trabalho pratico 1
Aluno: Isaque Fernando Moura da Silva
Matricula: 201712040022
Descricão do programa: Programa para gerar
uma arquivo de entrada aleatório.
O programa espera a inserção da capacidade
da mochila e também do número de itens.
Data: 19/04/18
Comando de compilação: gcc gerador_entrada.c -o gerador_entrada
****************************************/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(int argc, char const *argv[])
{
	int num_itens, cap_mochila;
	srand(time(0)); // Função para gerar núemros aleatórios diferentes.
	FILE *arq; // Ponteiro para arquivo de escrita, aonde vamos gerar nossos códigos.
	arq = fopen("entrada.txt", "w"); // Abrindo o arquivo ou criando caso não exita.

	if(arq == NULL){ // Verificação.
		printf("Erro ao abrir ou criar arquivo !!");
		return 1;
	}

	// Lendo os valores desejados pelo usuário.
	printf("Quantos itens deseja :");
	scanf("%d", &num_itens);
	printf("Qual a capacidade da sua mochila :");
	scanf("%d", &cap_mochila);

	// Printando as primeiras informações.
	fprintf(arq, "%d\n", cap_mochila); // Capacidade.
	printf("%d\n", cap_mochila); // Printando na tela a capacidade.
	fprintf(arq, "%d\n", num_itens); // Núemro de itens.
	printf("%d\n", num_itens); // Printando na tela o número de itens.

	// Gerando os itens.
	int i=0, peso_temp, valor_temp;
	while(i<num_itens){
		peso_temp = (rand() % 10) + 1; // Gerando números de 1 a 10
		valor_temp = (rand() % 10) + 1; // Gerando números de 1 a 10
		fprintf(arq, "%d %d\n", peso_temp, valor_temp); // Escrevendo no arquivo.
		printf("%d %d\n", peso_temp, valor_temp); // Printando na tela o peso e o valor.
		i++;
	}

	// Fechando o arquivo.
	fclose(arq);
	printf("Arquivo gerado e salvo !!\n");
	// No final teremos o que foi printado no arquivo também no terminal.
	return 0;
}