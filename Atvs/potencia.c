#include <stdio.h>
#include <stdlib.h>

int mult(int base, int exp){
	int cont = base;
	if(exp == 1){
		return base;
	}else if(exp == 0){
		return 1;
	}else{
		cont = cont * mult(base, exp-1);
		return cont;
	}
}

int main(){
	int base, exp;
	printf("Digite a base e o expoente inteiros: ");
	scanf("%d %d", &base, &exp);
	printf("Resultado: %d\n", mult(base, exp));
	return 0;
}