/* ========================================================================== */
#include <sys/time.h>
#include <stdio.h>
#include <stdlib.h>
#define INICIOARRANJO   1
#define MAXTAM          1000

typedef int TipoChave;

typedef int TipoApontador;

typedef struct {
  TipoChave Chave;
  /* outros componentes */
} TipoItem;

typedef struct {
  TipoItem Item[MAXTAM];
  TipoApontador Primeiro, Ultimo;
} TipoLista;

/* ========================================================================== */

void FLVazia(TipoLista *Lista)
{ Lista -> Primeiro = INICIOARRANJO;
  Lista -> Ultimo = Lista -> Primeiro;
}  /* FLVazia */

int Vazia(TipoLista Lista)
{ return (Lista.Primeiro == Lista.Ultimo);
}  /* Vazia */

void Insere(TipoItem x, TipoLista *Lista)
{ if (Lista -> Ultimo > MAXTAM) printf("Lista esta cheia\n");
  else { Lista -> Item[Lista -> Ultimo - 1] = x;
         Lista -> Ultimo++;
       }
}  /* Insere */

void Retira(TipoApontador p, TipoLista *Lista, TipoItem *Item)
{ int Aux;

  if (Vazia(*Lista) || p >= Lista -> Ultimo) 
  { printf(" Erro   Posicao nao existe\n");
    return;
  }
  *Item = Lista -> Item[p - 1];
  Lista -> Ultimo--;
  for (Aux = p; Aux < Lista -> Ultimo; Aux++)
    Lista -> Item[Aux - 1] = Lista -> Item[Aux];
}  /* Retira */

void Imprime(TipoLista Lista)
{ int Aux;

  for (Aux = Lista.Primeiro - 1; Aux <= (Lista.Ultimo - 2); Aux++)
    printf("%d\n", Lista.Item[Aux].Chave);
}  /* Imprime */

/* ========================================================================== */
// Funções implementadas por mim - Isaque Fernando
// Localizar um elemento na Lista pela chave.
// Vamos retornar a posição do elemento no arranjo.
int Localiza(TipoLista Lista, TipoChave chave){
	int i; // Na implementação dele começamos o arranjo em 0
	for(i=0; i<Lista.Ultimo; i++){
		if(Lista.Item[i].Chave == chave){
			return i; // Caso ache o elemento retornamos o indice.
		}
	}
	return -1; // Se não achar retornamos -1.
}

// Particionar a lista em dus a partir de determinado item.
TipoLista* Particiona(TipoLista *Lista, TipoChave chave){
	TipoLista *tip_lista = (TipoLista*) (malloc(sizeof(TipoLista))); // Alocando a nova lista.
	TipoItem *tip_item = (TipoItem*) malloc(sizeof(TipoItem)); // Alocando um novo item.
	TipoApontador *p = (TipoApontador*) malloc(sizeof(TipoApontador)); // Alocando espaço para um apontador.
	FLVazia(tip_lista); // Fazendo com que a lista fique vazia.
	int j = Localiza(*Lista, chave); // j recebe a posição inicial da nossa iteração.
	while(j!=Lista->Ultimo-1){ // Enquanto j não for retirado.
		*p = Lista->Ultimo-1; // Vamos retirar o elemento p-1; Precisamos ficar atentos pois o último aponta para duas casas afrente da que queremos retirar.
		Retira(*p, Lista, tip_item); // Retiramos o elemento e ajustamos o ultimo.
		Insere(*tip_item, tip_lista); // Inserimos o elemento retirado na nova lista.
	}
	// Desalocando o espaço.
	free(tip_item);
	free(p);
	printf("Lista :\n");
	Imprime(*Lista);
	printf("Lista aux:\n");
	Imprime(*tip_lista);
	return tip_lista; // Retornamos a nova lista.
}

int main(int argc, char *argv[]){ 
  int controle;
  TipoChave *tip_chave = (TipoChave*) malloc(sizeof(TipoChave)); // Alocando uma chave.
  TipoItem *tip_item = (TipoItem*) malloc(sizeof(TipoItem)); // Alocando um item.
  TipoLista *tip_list = (TipoLista*) malloc(sizeof(TipoLista)); // Alocando uma lista.
  TipoApontador *p = (TipoApontador*) malloc(sizeof(TipoApontador)); // Alocando um tipo apontador.
  while(1){
    printf("1. Criar lista vazia\n");
    printf("2. Inserir elemento\n");
    printf("3. Retirar elemento\n");
    printf("4. Localizar um item na lista dada a sua chave.\n");
    printf("5. Particionar a lista em duas a partir de um determinado item.\n");
    printf("6. Imprimir lista\n");
    printf("7. Sair\n");
    scanf("%d", &controle); // Obtendo o valor de controle.

    // Condicionais.
    if(controle==1){ // Criar lista vazia.
		FLVazia(tip_list); // Criando lista vazia.
		printf("Lista criada com sucesso\n");
	  	printf("\n");
	  	printf("\n");
  		printf("\n");
    }else if(controle==2){ // Inserir elemento.
		printf("Qual eh a chave do elemento: ");
		scanf("%d", &tip_item->Chave);
	    Insere(*tip_item, tip_list);
	    printf("\n");
  		printf("\n");
  		printf("\n");
    }else if(controle==3){ // Retirar elemento.
	    printf("Insira o elemento que deseja retirar: ");
	    scanf("%d", &p);
	    Retira(*p, tip_list, tip_item);
      	printf("\n");
  		printf("\n");
  		printf("\n");
    }else if(controle==4){ // Localizar item.
    	printf("Insira a chave do elemento que deseja localizar: ");
    	scanf("%d", &tip_chave);
    	if(Localiza(*tip_list, tip_chave) == -1){ // Caso o retorno seja igual a -1.
    		printf("Item nao encontrado!!\n");
    	}else{
    		printf("Item encontrado!!\n");
    	}
  		printf("\n");
  		printf("\n");
  		printf("\n");
    }else if(controle==5){ // Particionar a lista.
    	printf("Digite a chave do elemento que deseja utilizar para particionar: ");
    	scanf("%d", &tip_chave);
    	Particiona(tip_list, tip_chave);
  		printf("\n");
  		printf("\n");
  		printf("\n");
    }else if(controle==6){ // Imprimir lista.
    	Imprime(*tip_list);
  		printf("\n");
  		printf("\n");
  		printf("\n");
    }else if(controle==7){ // Sair
      return (0);
    }else{
      printf("Opcao incorreta!!\n");
    }
  }
}
