/****************************************
Laed1 - Trabalho pratico 1
Aluno: Isaque Fernando Moura da Silva
Matricula: 201712040022
Descricão do programa: Algoritmo guloso para buscar
a melhor solução para a mochila. O algoritmo consiste
em ler os valores e armazenar em um vetor temporário
que é ordenado pela razão dos elementos. Depois de 
ordenado coletamos as melhores razões. O algoritmo
espera um arquivo txt chamado entrada e retorna a 
escrita em um arquivo chamado saída.
Professora, o código para cálculo do tempo eu con-
segui com a Ana e o Rúbio. Não entendi bem essa fun-
ção.
Data: 19/04/18
Comando de compilação: gcc mochila_alg_guloso.c -o mochila
****************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <unistd.h>
#include <sys/resource.h>

// Estrutura do meu item. Precisamos de um valor, peso e uma razão.
typedef struct{
	int valor;
	int peso;
	int numero;
	float razao;
} Item;

// Estrutura da mochila, precisamos do total de elementos, peso da mochila e de um vetor para caber os itens.
typedef struct {
	int num_elemet;
	int peso_total_mochila;
	int soma_peso;
	int soma_valor;
	Item *itens;
} Mochila;

int main(int argc, char const *argv[])
{
	// Bloco para cálculo do tempo de execução.
	struct rusage usage;
    struct timeval now, start, end;
    double inicio, fim;

    getrusage (RUSAGE_SELF, &usage);
    start = usage.ru_utime;
    
    gettimeofday(&now,NULL);
    inicio = ((double) now.tv_usec)/1000000;
    inicio += ((double) now.tv_sec);
    // Fim do bloco inicial para cálculo do tempo.


	FILE *arq, *arq_escrita; // Ponteiro para tipo arquivo.
	arq = fopen("entrada.txt", "rt"); // Abrindo um arquivo para leitura.		
	arq_escrita = fopen("saida.txt", "w"); // Abrindo um arquivo para escrida do resultado.

	if(arq==NULL || arq_escrita==NULL){ // Pequena exceção para verificar a abertura do arquivo.
		printf("Problema para abrir o arquivo !!\n");
	}

	// Criando meu item e mochila.
	Mochila *mochila = (Mochila*) malloc(sizeof(Mochila));
	Item *item = (Item*) malloc(sizeof(Item));

	// Vamos ler as primeiras linhas do arquivo.
	int peso_mochila, num_itens;
	fscanf(arq, "%d", &peso_mochila);
	fscanf(arq, "%d", &num_itens);
	
	// Alocando espaço em meu vetor de itens da mochila.
	mochila->itens = (Item*) (malloc(num_itens*sizeof(Item))); // A mochila precisa caber num_itens.
	mochila->peso_total_mochila = peso_mochila; // Atribuição do peso total da mochila.
	mochila->num_elemet = 0; // Controle do número de elementos.
	mochila->soma_peso = 0;
	mochila->soma_valor = 0;
	// Precisamos de um vetor auxiliar para não colocar na todos os elementos e depois retirar.
	Item *itens_aux = (Item*) (malloc(num_itens*sizeof(Item))); 

	// Lendo meus itens e amarzenando.
	int i=0, peso_item, valor_item; // Variável para utilizarmos como índice.
	while(i<num_itens){
		// Lendo e guardando os valores do item no vetor auxiliar.
		fscanf(arq, "%d %d", &peso_item, &valor_item);
		itens_aux[i].numero = i+1; // Id do item.
		itens_aux[i].peso = peso_item;
		itens_aux[i].valor = valor_item;
		itens_aux[i].razao = (float) itens_aux[i].valor / itens_aux[i].peso; // Obtendo a razão do item.
		i++;
	}
	
	// Lidos os valores precisamos ordenar o vetor de itens com as melhores razões.
	int j=0;
	Item *aux = (Item*) malloc(sizeof(Item)); // Item auxiliar para troca.
	// Booble sort para ordenação decrescente.
	for(i=0; i<num_itens; i++){ // Iremos ordenar de maneira crescente.
		for(j=1; j<num_itens-1; j++){
			if(itens_aux[j].razao < itens_aux[j+1].razao){
				*aux = itens_aux[j];
				itens_aux[j] = itens_aux[j+1];
				itens_aux[j+1] = *aux;
			}
		}
	}

	// Com o vetor ordenado vamos pegar as melhores razões sem exceder o peso.
	i=0;
	int peso_temp;
	while(i<num_itens){
		// Caso a soma do peso da mochila mais o peso do item da iteração seja maior 
		peso_temp = mochila->soma_peso + itens_aux[i].peso; 
		if(peso_temp >= mochila->peso_total_mochila){
			break; // Se o peso exceder ou chegar no limite paramos o laço.
		}
		mochila->itens[i] = itens_aux[i]; // Atribuindo item.
		mochila->soma_peso = mochila->soma_peso + mochila->itens[i].peso; // Somando o peso.
		mochila->soma_valor = mochila->soma_valor + mochila->itens[i].valor; // Somando o valor.
		mochila->num_elemet++; // Aumentando a contagem de elementos.

		// Printando os valores
		fprintf(arq_escrita, "Id item: %d  ", mochila->itens[i].numero);
		fprintf(arq_escrita, "Peso do item: %d  ", mochila->itens[i].peso);
		fprintf(arq_escrita, "Valor do item: %d\n", mochila->itens[i].valor);
		// Printando o somatório dos pesos e valor
		fprintf(arq_escrita, "Somatorio dos pesos: %d\n", mochila->soma_peso);
		fprintf(arq_escrita, "Somatorio dos valores: %d\n", mochila->soma_valor);
		i++;
	}

	// Depois de buscar a solução precisamos do tempo.
	// Inicio do bloco para o cálculo do tempo.
	gettimeofday(&now,NULL);
    fim = ((double) now.tv_usec)/1000000 ;
    fim += ((double) now.tv_sec);

    getrusage(RUSAGE_SELF, &usage);
    end = usage.ru_utime;
    
    printf("Tempos gastos: \n");
    printf("Tempo de computação: %.4f \n", fim - inicio);
    printf("Tempos de entrada e saída: %ld.%lds\n",  end.tv_sec-start.tv_sec,  end.tv_usec-start.tv_usec);
    // Fim do bloco para cálculo de tempo.

	fclose(arq_escrita); // Fechando os arquivos.
	fclose(arq);
	return 0;
}