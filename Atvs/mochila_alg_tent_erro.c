/****************************************
Laed1 - Trabalho pratico 1
Aluno: Isaque Fernando Moura da Silva
Matricula: 201712040022
Descricão do programa: Algoritmo tentativa e erro.
Tomei como base a combinação binária para realizar as 
combinações. A ideia base é imaginar que cada casa representa
um item que pode estar ou naão na mochila. Apos utilizar isso busquei
colocar os itens na mochila e ir comparando resultado a resultado para
obter o melhor.
Data: 19/04/18
Comando de compilação: gcc mochila_alg_tent_erro.c -lm -o mochila_alg_tent_erro
****************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <unistd.h>
#include <sys/resource.h>
#include <math.h>

// Estrutura do item.
typedef struct { // Nessa nova estrutura não precisaremos da razão.
	int valor;
	int peso;
	int numero; // Identificador
} Item;

// Estrutura da mochila.
typedef struct{
	int num_element;
	int peso_total_mochila;
	int soma_peso;
	int soma_valor;
	Item *itens;
} Mochila;

// Função para printar um item.
void printItem(Item i, FILE *arq){
	fprintf(arq, "Id item: %d\n", i.numero);
	fprintf(arq, "Peso item: %d\n", i.peso);
	fprintf(arq, "Valor item: %d\n", i.valor);
}

// Função para iniciar o somador zerado.
void zerarVetorBinario(int *vet, int quant_bits){
	int i;
	for(i=0; i<quant_bits; i++){
		vet[i]=0;
	}
}

// Função para printar vetor binário
void printarVetorBinario(int vet[], int quant_bits){
	int i;
	for(i=0; i<quant_bits; i++){
		printf("%d  ", vet[i]);
	}
	printf("\n");	
}

// Função para realizar a soma binária.
void somaBinaria(int *vet, int quant_bits){
	int i, somaTemp, Transp;
	for(i=0; i<quant_bits; i++){ // Precisamos ter cuidado com a primeira casa e a última também.
		if(i==0){ // Na primeira iteração teremos Transp = 1 para realizar a soma.
			Transp=1; // Realizará a adição do valor 1 a cada chamada.
			somaTemp = vet[i]+Transp;
			if(somaTemp == 2){ // Caso do 1 + 1.
				vet[i]=0;
				Transp=1;
			}else if(somaTemp == 1){ // Caso o resultado seja 1.
				vet[i]=1;
				Transp=0;
			}else{
				vet[i]=0;
				Transp=0;
			}
		}else{ // A única mudança que teremos aqui é que Transp depende da iteração anterior.
			somaTemp = vet[i]+Transp; // Soma comum.
			if(somaTemp == 2){ // Caso do 1 + 1.
				vet[i]=0;
				Transp=1;
			}else if(somaTemp == 1){ // Caso o resultado seja 1.
				vet[i]=1;
				Transp=0;
			}else{
				vet[i]=0;
				Transp=0;
			}
		}
	}
}

int main(int argc, char const *argv[]){
	// Bloco para cálculo do tempo de execução.
	struct rusage usage;
    struct timeval now, start, end;
    double inicio, fim;

    getrusage (RUSAGE_SELF, &usage);
    start = usage.ru_utime;
    
    gettimeofday(&now,NULL);
    inicio = ((double) now.tv_usec)/1000000;
    inicio += ((double) now.tv_sec);
    // Fim do bloco inicial para cálculo do tempo.

    FILE *arq, *arq_escrita; // Ponteiro para tipo arquivo.
	arq = fopen("entrada.txt", "rt"); // Abrindo um arquivo para leitura.		
	arq_escrita = fopen("saida.txt", "w"); // Abrindo um arquivo para escrida do resultado.

	if(arq==NULL || arq_escrita==NULL){ // Pequena exceção para verificar a abertura do arquivo.
		printf("Problema para abrir o arquivo !!\n");
	}

	// Criando meu item e mochila.
	Mochila *mochila = (Mochila*) malloc(sizeof(Mochila));
	Mochila *mochila_aux = (Mochila*) malloc(sizeof(Mochila)); 
	Item *item = (Item*) malloc(sizeof(Item));

	// Vamos ler as primeiras linhas do arquivo.
	int peso_mochila, num_itens;
	fscanf(arq, "%d", &peso_mochila);
	fscanf(arq, "%d", &num_itens);

	// Alocando espaço em meu vetor de itens da mochila.
	mochila->itens = (Item*) (malloc(num_itens*sizeof(Item))); // A mochila precisa caber num_itens.
	mochila->peso_total_mochila = peso_mochila; // Atribuição do peso total da mochila.
	mochila->num_element = 0; // Controle do número de elementos.
	mochila->soma_peso = 0;
	mochila->soma_valor = 0;
	// Mochila auxiliar.
	mochila_aux->itens = (Item*) (malloc(num_itens*sizeof(Item))); // Precisamos de uma mochila auxiliar.
	mochila_aux->peso_total_mochila = peso_mochila; // Não vamos precisar deste campo.
	mochila_aux->num_element = 0;
	mochila_aux->soma_peso = 0;
	mochila_aux->soma_valor = 0;


	// Precisamos de um vetor auxiliar para não colocar na todos os elementos e depois retirar.
	Item *itens_aux = (Item*) (malloc(num_itens*sizeof(Item)));

	// Lendo meus itens e amarzenando.
	int i=0, peso_item, valor_item; // Variável para utilizarmos como índice.
	while(i<num_itens){
		// Lendo e guardando os valores do item no vetor auxiliar.
		fscanf(arq, "%d %d", &peso_item, &valor_item);
		itens_aux[i].numero = i+1; // Id do item.
		itens_aux[i].peso = peso_item;
		itens_aux[i].valor = valor_item; // Da mesma forma, não precisaremos da razão nesse caso.
		i++;
	}

	// Até este o ponto o código dos dois métodos são iguais, porém neste momento usaremos uma lógica de processamento
	// diferente. Vamos nos basear na combinação binária.
	// Dado um vetor de n elementos todas as combinações são possíveis imaginando que o estado 1 é estar na mochila e 0
	// não estar na mochila. Meu vetor terá n casas possibilitando n^2 combinações.
	int *vetBinario = (int*) malloc(num_itens*sizeof(int)); // Temos nosso vetor de n bits.

	// Conferindo a soma binária.
	zerarVetorBinario(vetBinario, num_itens); // Zerando o vetor.
	// Agora precisamos fazer as 2^n combinações possíveis.
	int comb_possiveis = pow(2, num_itens);
	int j=0; // j servirá como indice de busca interna.
	i=0;
	while(i<comb_possiveis){ // A cada iteração temos uma combinação possível.
		somaBinaria(vetBinario, num_itens); // Somar mais 1 no vetor.
		// Nesse ponto temos dois vetores, o de itens e também de números binários.
		// Vamos buscar posição por posição e pegar os valores que sua posição binária estiver em nível alto.
		for(j=0; j<num_itens; j++){
			if(vetBinario[j]==1){ // Comparação de valor da casa binária.
				mochila_aux->itens[j] = itens_aux[j]; // Caso o valor seja alto pegamos o item.
				mochila_aux->soma_peso = mochila_aux->soma_peso + mochila_aux->itens[j].peso; // Somando o peso.
				mochila_aux->soma_valor = mochila_aux->soma_valor + mochila_aux->itens[j].valor; // Somando o valor.
				mochila_aux->num_element++; // Aumentando a contagem de elementos.

				fprintf(arq_escrita, "Id item: %d  ", mochila_aux->itens[j].numero);
				fprintf(arq_escrita, "Peso do item: %d  ", mochila_aux->itens[j].peso);
				fprintf(arq_escrita, "Valor do item: %d\n", mochila_aux->itens[j].valor);
				// Printando o somatório dos pesos e valor
				fprintf(arq_escrita, "Somatorio dos pesos: %d\n", mochila_aux->soma_peso);
				fprintf(arq_escrita, "Somatorio dos valores: %d\n", mochila_aux->soma_valor);
			}
		}
		// Depois de pegarmos a tentativa vemos se ela realmente é a melhor dentro do peso aceitável
		// Comparamos se o peso da mochila auxiliar cabe dentro da mochila e também se o valor é maior que o que ela possui atualmente.
		if(mochila_aux->soma_peso<=mochila->peso_total_mochila && mochila_aux->soma_valor>mochila->soma_valor){
			*mochila = *mochila_aux; // mochila recebe a auxiliar.
		}
		// Precisamos zerar os valores da mochila auxiliar para uma nova iteração.
		mochila_aux->peso_total_mochila = peso_mochila; // Não vamos precisar deste campo.
		mochila_aux->num_element = 0;
		mochila_aux->soma_peso = 0;
		mochila_aux->soma_valor = 0;
		i++;
	}
	// Impressão do resultado final.
	fprintf(arq_escrita, "\n\n\n");
	fprintf(arq_escrita, "Resultado Final\n");
	fprintf(arq_escrita, "Itens na mochila\n");
	fprintf(arq_escrita, "Numero de elemntos na mochila: %d\n", mochila->num_element);
	for(j=0; j<mochila->num_element; j++){
		printItem(mochila->itens[j], arq_escrita);
	}
	fprintf(arq_escrita, "Peso total da mochila: %d\n", mochila->soma_peso);
	fprintf(arq_escrita, "Valor total da mochila: %d\n", mochila->soma_valor);

	// Inicio do bloco para o cálculo do tempo.
	gettimeofday(&now,NULL);
    fim = ((double) now.tv_usec)/1000000 ;
    fim += ((double) now.tv_sec);

    getrusage(RUSAGE_SELF, &usage);
    end = usage.ru_utime;
    
    printf("Tempos gastos: \n");
    printf("Tempo de computação: %.4f \n", fim - inicio);
    printf("Tempos de entrada e saída: %ld.%lds\n",  end.tv_sec-start.tv_sec,  end.tv_usec-start.tv_usec);
    // Fim do bloco para cálculo de tempo.

    fclose(arq_escrita);
    fclose(arq);
	return 0;
}