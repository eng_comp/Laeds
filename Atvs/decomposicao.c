#include <stdio.h>
#include <stdlib.h>

// Preenche o vetor de zeros.
void preencheVetorNulo(int v[], int tamVetor){ 
	int i;
	for(i=0; i<tamVetor; i++){
		v[i]=0;
	}
}

// Impressão do vetor.
void imprimeVetor(int v[], int tamVetor){ 
	int i;
	printf("%d ", v[0]); // Imprime o primeiro elemento.
	for(i=1; i<tamVetor; i++){
		if(v[i] != 0){ // Se o valor for diferente de zero.
			printf("+ %d ", v[i]);
		}
	}
	printf("\n"); // No final adicionamos uma linha.
}

// Caso onde o pivo será repetido.
void preencheVetor(int v[], int n, int valor, int tamVetor){ 
	int i;
	for(i=0; i<n; i++){ // Vamos preencher o vetor com os valores iguais até a soma ser igual ao numero que estamos decompondo.
		v[i] = valor;
	}
	if(n != tamVetor){ // Caso n seja diferente de tamVetor preenchemos o restante com zeros.
		for(i=n; i<tamVetor; i++){
			v[i] = 0;
		}
	}
}

// Caso onde a divisão é maior que 2 e o resto é diferente de zero.
void preencheVetorComResto(int v[], int n, int valor, int tamVetor, int resto){
	int i;
	for(i=0; i<n; i++){ // Vamos preencher o vetor com os valores iguais até a soma ser igual ao numero que estamos decompondo.
		v[i] = valor;
	}
	v[n] = resto; // Aqui na última posição colocamos o resto.
	if(n != tamVetor){ // Caso n seja diferente de tamVetor preenchemos o restante com zeros. Vamos começar de n+1.
		for(i=n+1; i<tamVetor; i++){
			v[i] = 0;
		}
	}	
}

// Vamos acrescentar um no primeiro elemento do vetor que seja igual a zero.
void acrescentaUm(int v[], int tamVetor){
	int i;
	for(i=0; i<tamVetor; i++){
		if(v[i] == 0){ // Se há uma posição nula.
			v[i] = 1; // Preenchemos com 1.
			break; // Depois de colocarmos o 1, saímos do laço.
		}
	}
}

// Onde n é o número que desejamos decompor.
void decomposicao(int n){ 
	int v[n], i; // Vetor de n posições. A maior decomposição é o número n representado da for 1 + 1 + 1 ... + 1 = n.
	int pivo; // Primeiro número do vetor. Usado na decomposição.
	if(n == 1){ // Caso o número seja igual a 1.
		printf("%d\n", n); // Printamos 1 e terminamos o programa.
	}else{
		printf("%d\n", n); // Printamos o número que iremos decompor.
		preencheVetorNulo(v, n); // Preenchendo nosso vetor.
		pivo = n - 1; // Pegamos o primeiro pivo.
		v[0] = pivo; // Nosso primeiro caso envolve o vetor v na primeira posição com o número que iremos decompor.
		v[1] = 1; // Primeiro caso.
		imprimeVetor(v, n); // Imprimimos a primeira vez.
		// Continuação utilizando o algoritmo.
		while(v[0] != 1){ // Enquanto nosso pivo não for igual a 1 o loop continua.
			for(i=n-1; i>0; i--){ // Laço para percorrer o vetor e procurar pelo fator de decomposição. Precisamos decrementar.
				if(v[i]>1 && v[i]<4){ // Caso haja algum valor maior que 1 e menor que 4 vamos decompor o valor.
					v[i] = v[i] - 1; // Decomposição do número.
					acrescentaUm(v, n); // Acrescentamos um no vetor.
					imprimeVetor(v, n); // Imprime os valores positivos.
					i=n; // Voltamos ao passo inicial. Lembrando que no final do laço teremos uma subtração.
				}else{ // Logo v[i] é um valor maior que 4 que precisa ser decomposto.
					int novoN = v[i]; // Terei agora um novo n.
					v[i+1] = 1; // Como já temos o nosso novo pivo no local correto adicionamos 1.
					imprimeVetor(v, n); // Imprimo o vetor com a nova decomposição adicionada.
					int divisao = n/pivo; // Vamos precisar desse valor.
					int restDivisao = n%pivo; // Precisamos do resto para conferir a exatidão.
					
					pivo = pivo - 1; // Novo pivo é igual ao antigo menos 1;
					preencheVetorNulo(v, n); // Zeramos o vetor.
					int divisao = n/pivo; // Vamos precisar desse valor.
					int restDivisao = n%pivo; // Precisamos do resto para conferir a exatidão.
					if(divisao >= 2 && restDivisao == 0){ // Divisão exata com resultado maior que 2.
						preencheVetor(v, divisao, pivo, n); // O resultado da divisão revela o número de elementos que vou colocar no vetor igual ao pivo.
						imprimeVetor(v, n); // Impressão do vetor.
					}else if(divisao >= 2 && restDivisao != 0){ // Caso onde o resto é igual a 1;
						preencheVetorComResto(v, divisao, pivo, n, restDivisao); // Utilizo a função especifica para o preenchimento.
						imprimeVetor(v, n); // Imprimindo e sendo feliz. 
					}else{ // Caso não ocorra esse fato precisamos de usar uma ferramenta algebrica.
						int proxElemento = n - pivo; // Valor para descobrir a próximo elemento para decomposição básica.
						v[0] = pivo; // Primeiro elemento é o pivo.
						v[1] = proxElemento; // Segundo elemento é o próximo elemento.	
						imprimeVetor(v, n);
					}					
				}
			}
			pivo = pivo - 1; // Novo pivo é igual ao antigo menos 1;
			preencheVetorNulo(v, n); // Zeramos o vetor.
			int divisao = n/pivo; // Vamos precisar desse valor.
			int restDivisao = n%pivo; // Precisamos do resto para conferir a exatidão.
			if(divisao >= 2 && restDivisao == 0){ // Divisão exata com resultado maior que 2.
				preencheVetor(v, divisao, pivo, n); // O resultado da divisão revela o número de elementos que vou colocar no vetor igual ao pivo.
				imprimeVetor(v, n); // Impressão do vetor.
			}else if(divisao >= 2 && restDivisao != 0){ // Caso onde o resto é igual a 1;
				preencheVetorComResto(v, divisao, pivo, n, restDivisao); // Utilizo a função especifica para o preenchimento.
				imprimeVetor(v, n); // Imprimindo e sendo feliz. 
			}else{ // Caso não ocorra esse fato precisamos de usar uma ferramenta algebrica.
				int proxElemento = n - pivo; // Valor para descobrir a próximo elemento para decomposição básica.
				v[0] = pivo; // Primeiro elemento é o pivo.
				v[1] = proxElemento; // Segundo elemento é o próximo elemento.
				imprimeVetor(v, n);
			}
		}
	}
}

int main(int argc, char const *argv[]){
	int num;
	printf("Digite um numero inteiro: ");
	scanf("%d", &num);
	printf("Decomposicao:\n");
	decomposicao(num);
	return 0;
}