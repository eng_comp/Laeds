#include <stdio.h>

int Digitos(int N){
	int cont = 1;
	if(N < 10){
		return cont;
	}else{
		N = N / 10;
		cont = cont + Digitos(N);
		return cont;
	}
	
} 

int main(){
	int n;
	printf("Digite um numero inteiro: ");
	scanf("%d", &n);
	printf("Resultado: %d\n", Digitos(n));
}