// Fonte: https://gist.github.com/marcoscastro/60f8f82298212e267021
#include <stdio.h>

// Realiza a troca de elementos com os indices passados como parâmetro.
void troca(int vetor[], int i, int j){
	int aux = vetor[i];
	vetor[i] = vetor[j];
	vetor[j] = aux;
}

// Função recursiva de permutação.
void permuta(int vetor[], int inf, int sup){
	if(inf == sup){ // Caso o limite inferior seja igual ao superior.
		for(int i = 0; i <= sup; i++) // For até o limite superior.
			printf("%d ", vetor[i]);
		printf("\n");
	}
	else{
		for(int i = inf; i <= sup; i++){
			troca(vetor, inf, i);
			permuta(vetor, inf + 1, sup);
			troca(vetor, inf, i); // backtracking
		}
	}
}

int main(int argc, char *argv[]){
	int v[] = {1, 2, 3, 4};
	int tam_v = sizeof(v) / sizeof(int);

	permuta(v, 0, tam_v - 1);

	return 0;
}