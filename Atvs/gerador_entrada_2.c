/****************************************
Laed1 - Trabalho pratico 1
Aluno: Isaque Fernando Moura da Silva
Matricula: 201712040022
Descricão do programa: Programa para gerar
uma arquivo de entrada aleatório.
O programa espera a inserção da capacidade
da mochila e também do número de itens.
Este programa obdece o peso W proposto pela professora.
Data: 19/04/18
Comando de compilação: gcc gerador_entrada.c -o gerador_entrada
****************************************/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(int argc, char const *argv[])
{
	srand(time(0)); // Função para gerar números aleatórios diferentes.
	int num_itens, cap_mochila, soma_peso=0;
	int *vet_pesos, *vet_valores; // Vetores para armazenar os pesos e valores gerados.
	
	FILE *arq; // Ponteiro para arquivo de escrita, aonde vamos gerar nossos códigos.
	arq = fopen("entrada.txt", "w"); // Abrindo o arquivo ou criando caso não exita.

	if(arq == NULL){ // Verificação.
		printf("Erro ao abrir ou criar arquivo !!");
		return 1;
	}

	// Lendo os valores desejados pelo usuário.
	printf("Quantos itens deseja :");
	scanf("%d", &num_itens);

	// Alocando dinâmicamente os vetores de peso e valor.
	vet_pesos = (int*) malloc(num_itens*sizeof(int));
	vet_valores = (int*) malloc(num_itens*sizeof(int));

	// Gerando os itens.
	int i=0, peso_temp, valor_temp;
	while(i<num_itens){
		peso_temp = (rand() % 10) + 1; // Gerando números de 1 a 10
		valor_temp = (rand() % 10) + 1; // Gerando números de 1 a 10
		// Guardando os valores.
		vet_valores[i] = valor_temp;
		vet_pesos[i] = peso_temp;
		// Soma vai armazendo o somatório de pesos.
		soma_peso = soma_peso + peso_temp;
		i++;
	}

	// Organizando e printando.
	cap_mochila = (int) (soma_peso * 0.5); // Obtendo os 50% do somatório de pesos.
	// Printando as primeiras informações.
	fprintf(arq, "%d\n", cap_mochila); // Capacidade.
	printf("Capacidade da mochila (W): %d\n", cap_mochila);
	fprintf(arq, "%d\n", num_itens); // Número de itens.
	i=0;
	while(i<num_itens){
		fprintf(arq, "%d %d\n", vet_pesos[i], vet_valores[i]); // Escrevendo no arquivo.
		i++;
	}

	// Fechando o arquivo.
	fclose(arq);
	free(vet_pesos);
	free(vet_valores);
	printf("Arquivo gerado e salvo !!\n");
	// No final teremos o que foi printado no arquivo também no terminal.
	return 0;
}