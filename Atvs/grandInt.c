#include <stdio.h>
#include <stdlib.h>

void printGrandInt(int grandInt[], int controle);

int main(){
	int grandInt[36000];
	int carryOut = 0, mult = 0;
	int num, i, j, k = 0, controle = 0;
	grandInt[0] = 1;

	printf("Digite um numero inteiro: ");
	scanf("%d", &num);

	// Preenchendo o vetor com 0.
	for(i=1; i<36000; i++){
		grandInt[i]=0;
	}

	// Tentativa do fatorial.
	for(i=1; i<=num; i++){ // Para cada número.
		carryOut = 0;
		for(j=0; j<36000; j++){ // Vou pegar o elemento
			mult = (grandInt[j] * i) + carryOut; // Multiplicação do grandInt[j] pelo indice do for externo.
			//if(mult >= 10){ // Se o resultado for maior que 10.
				carryOut = mult/10; // Carryout anda uma casa no mult.
				mult = mult%10; // mult recebe apenas a ultima casa.
			//}
			grandInt[j] = mult; // Armazenamos a casa.
			if(grandInt[j+1] == 0 && carryOut == 0 && (j+1)>=k){ // Se o proximo número for igual a zero e o carryOut também:
				controle = j+1; // Controle recebe o valor de k+1;
				break; // paramos o laço.
			}
		}
		k = j;
	}

	printf("Resultado: ");
	printGrandInt(grandInt, controle);
	printf("\n");
}

void printGrandInt(int grandInt[], int controle){
	int i;
	for(i=controle-1; i>=0; i--){ // Imprimir o vetor de trás para frente.
		printf("%d", grandInt[i]);
	}
}
