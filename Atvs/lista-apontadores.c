#include <sys/time.h>
#include <stdlib.h>
#include <stdio.h>
#define MAX 10

/* ========================================================================= */

typedef int TipoChave;

typedef struct {
    int Chave;
    /* outros componentes */
} TipoItem;

typedef struct TipoCelula *TipoApontador;

typedef struct TipoCelula {
    TipoItem Item;
    TipoApontador Prox;
} TipoCelula;

typedef struct {
    TipoApontador Primeiro, Ultimo;
} TipoLista;

/* ========================================================================= */

void FLVazia(TipoLista *Lista){ 
    Lista -> Primeiro = (TipoApontador) malloc(sizeof(TipoCelula)); // Primeiro recebe uma posição aleatória de memória.
    Lista -> Ultimo = Lista -> Primeiro; // Lista->Ultimo e Lista->Primeiro apontam para a mesma posição.
    Lista -> Primeiro -> Prox = NULL; // Lista->Primeiro->Prox recebe nulo.
}

int Vazia(TipoLista Lista){ 
    return (Lista.Primeiro == Lista.Ultimo); // Caso Lista->Primeiro e Lista->Ultima forem iguais a lista está vazia.
}

void Insere(TipoItem x, TipoLista *Lista){ 
    Lista -> Ultimo -> Prox = (TipoApontador) malloc(sizeof(TipoCelula)); // Lista->Ultimo->Prox recebe uma posição de memória para a célula.
    Lista -> Ultimo = Lista -> Ultimo -> Prox; // Ultimo passa a apontar para a nova célula criada.
    Lista -> Ultimo -> Item = x; // No item da nova célula guardamos o valor de x.
    Lista -> Ultimo -> Prox = NULL; // Prox recebe null.
}

void Retira(TipoApontador p, TipoLista *Lista, TipoItem *Item){ 
    /*  ---   Obs.: o item a ser retirado e  o seguinte ao apontado por  p --- */
    TipoApontador q; // Variável para uma célula.
    // Caso a lista esteja vazia ou p seja uma célula vazia ou p->prox seja uma célula vazia.
    if (Vazia(*Lista) || p == NULL || p -> Prox == NULL){ 
        printf(" Erro Lista vazia ou posicao nao existe\n");
        return;
    }
    q = p -> Prox; // q passa a apontar para p->Prox.
    *Item = q -> Item; // O conteúdo de Item recebe q->Item.
    p -> Prox = q -> Prox; // p->Prox agora aponta para o elemento depois de p+1.
    if (p -> Prox == NULL) Lista -> Ultimo = p; // Caso o proximo elemento de p seja nulo o último recebe p.
    // Caso contrario o último continua sendo o último.
    free(q);
}

void Imprime(TipoLista Lista){ 
    TipoApontador Aux; // Apontador auxiliar.
    Aux = Lista.Primeiro -> Prox; // Aux recebe o elemento após a célula cabeça.
    while (Aux != NULL) { // Enquanto Aux não for nulo.
        printf("%d\n", Aux -> Item.Chave); // Printamos o item.
        Aux = Aux -> Prox; // E Aux recebe o endereço da próxima célula proximo item.
    }
}

/* ========================================================================== */
// Implementações das funções que irei utilizar.
// Função para localizar elemento e retornar seu endereço.
TipoApontador Localiza(TipoLista *Lista, TipoChave chave){
    TipoApontador Aux; // Um tipo célula.
    Aux = Lista->Primeiro->Prox; // Aux agora aponta para a primeira célula com elemento.
    while(Aux!=NULL){
        if(Aux->Item.Chave == chave){ // Caso o valor seja igual a chave.
            return Aux; // Retornamos.
        }
        Aux = Aux->Prox; // Apontamos para o próximo elemento.
    }
    // Atribuição antes de sair.
    return NULL; // Caso contrário retornamos null.
}

// Função para particionar a lista.
TipoLista* Particiona(TipoLista *Lista, TipoChave chave){
    TipoLista *tip_list = (TipoLista*) malloc(sizeof(TipoLista)); // Lista auxiliar.
    TipoApontador Aux; // Variável TipoCélula.
    FLVazia(tip_list); // Fazendo lista vazia.
    Aux = Localiza(Lista, chave); // Buscando pelo item que irá proporcionar a partição.
    if(Aux==NULL){
        printf("Item não encontrado !!\n");
        return NULL;
    }
    tip_list->Primeiro->Prox = Aux->Prox; // O primeiro item da lista auxiliar é Aux->Prox.
    tip_list->Ultimo = Lista->Ultimo; // O ultimo elemento da lista auxiliar é o mesmo da lista antiga.
    Lista->Ultimo = Aux; // O último item da Lista agora aponta para onde Aux aponta.
    Lista->Ultimo->Prox = NULL; // Fazendo com que o proximo item depois de último seja null.
    //free(Aux); // Cuidado com o comando free.
    return tip_list; // Retornamos a nova lista.
}

int main(int argc, char *argv[]){
    int controle, chave;
    TipoLista *tip_list = (TipoLista*) malloc(sizeof(TipoLista));
    TipoItem *tip_item = (TipoItem*) malloc(sizeof(TipoItem));
    // TipoChave *tip_chave = (TipoChave*) malloc(sizeof(TipoChave));
    TipoApontador *p = (TipoApontador*) malloc(sizeof(TipoApontador));
    while(1){
        printf("1. Criar lista vazia\n");
        printf("2. Inserir elemento\n");
        printf("3. Retirar elemento\n");
        printf("4. Localizar um item na lista dada a sua chave.\n");
        printf("5. Particionar a lista em duas a partir de um determinado item.\n");
        printf("6. Imprimir lista\n");
        printf("7. Sair\n");
        scanf("%d", &controle); // Obtendo o valor de controle.

        // Condicionais.
        if(controle==1){ // Criar lista vazia.
            FLVazia(tip_list); // Criando lista vazia.
            printf("Lista criada com sucesso\n");
            printf("\n");
            printf("\n");
            printf("\n");
        }else if(controle==2){ // Inserir elemento.
            printf("Qual eh a chave do elemento: ");
            scanf("%d", &tip_item->Chave);
            Insere(*tip_item, tip_list);
            printf("\n");
            printf("\n");
            printf("\n");
        }else if(controle==3){ // Retirar elemento.
            printf("Insira o elemento que deseja retirar: ");
            scanf("%d", &chave);
            p = Localiza(tip_list, chave); // Localizamos o item que desejamos retirar.
            Retira(p, tip_list, tip_item); // Retiramos o elemento depois de p.
            printf("\n");
            printf("\n");
            printf("\n");
        }else if(controle==4){ // Localizar item.
            printf("Insira a chave do elemento que deseja localizar: ");
            scanf("%d", &chave);
            p = Localiza(tip_list, chave); // Buscando pelo elemento.
            if(p==NULL){ // Caso seja verdadeiro.
                printf("Item nao encontrado!!\n");
            }else{
                printf("Item encontrado!!\n");
            }
            printf("\n");
            printf("\n");
            printf("\n");
        }else if(controle==5){ // Particionar a lista.
            TipoLista *tip_lista = (TipoLista*) malloc(sizeof(TipoLista)); // Lista auxiliar.
            printf("Digite a chave do elemento que deseja utilizar para particionar: ");
            scanf("%d", &chave);
            tip_lista = Particiona(tip_list, chave); // A função retorna a outra lista.
            printf("Lista :\n");
            Imprime(*tip_list); // Printamos a lista do programa.
            printf("Lista Auxiliar:\n");
            Imprime(*tip_lista); // Printamos a lista auxiliar.
            free(tip_lista);
            printf("\n");
            printf("\n");
            printf("\n");
        }else if(controle==6){ // Imprimir lista.
            Imprime(*tip_list);
            printf("\n");
            printf("\n");
            printf("\n");
        }else if(controle==7){ // Sair
            return (0);
        }else{
            printf("Opcao incorreta!!\n");
        }
    }
    return(0);
}